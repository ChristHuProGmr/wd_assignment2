/**
 * validates if base, target and amount are valid
 * @returns boolean
 */
function validateCurrency(){
    let baseCur   = document.getElementById('base_currency_select').value
    let targetCur = document.getElementById('to_currency_select').value
    let amount = document.getElementById('amount').value;
    if(baseCur==targetCur || amount=="" || amount<1){
        return false;
    }
    return true;
}

/**
 * removes the table and replaces it with a new one
 */
function killTable(){
    let headers = document.getElementsByTagName('tr')[0];
	document.getElementsByTagName('table')[0].remove();
	createTagElement('table', document.getElementById('main-section'));
	document.getElementsByTagName('table')[0].appendChild(headers);
}

/**
 * adds the newly processed query at the end of the table
 * @param {object} data 
 */
function displayResults(data){
    let table = document.getElementsByTagName('table')[0];
	createTagElement('tr', table);

    let tdIndex = 0;
    let rows = table.getElementsByTagName('tr');
    Object.values(data).forEach(value=>{
        createTagElement('td', rows[rows.length - 1]);
		rows[rows.length - 1].getElementsByTagName('td')[tdIndex].textContent = value;
        tdIndex++;
    })
}

/**
 * fetches currency data stored locally and calls a method to
 * initialize select options
 */
function loadCurrencyData(){
    let url = 'https://gist.githubusercontent.com/ksafranski/2973986/raw/5fda5e87189b066e11c1bf80bbfbecb556cf2cc1/Common-Currency.json';
    fetch(url).then(resp=>{
        if(resp.status === 200){
            console.log('success');
            return resp.json();
        }
        throw new Error('failed to fetch currency data');
    }).then(currencyCollection=>{
        initializeSelectOptions(currencyCollection);
    }).catch(err=>{
        console.log('oh no', err);
    })
}

/**
 * initialze options for both select fields
 * populated with data from the currency json file 
 * @param {object} currencyCollection 
 */
function initializeSelectOptions(currencyCollection){
    let selectFields = getSelectIDs();
    selectFields.forEach(field=>{
        createOptions(field, currencyCollection);
    })
}

/**
 * get the elements of the select fields
 * then return them inside an array
 * @returns array
 */
function getSelectIDs(){
    let base = document.getElementsByTagName('select')[0];
    let target = document.getElementsByTagName('select')[1];
    return [base, target];
}

/**
 * creates option tags for a given field then sets the required attributes
 * @param {HTML element} field 
 * @param {object} currencyCollection 
 */
function createOptions(field, currencyCollection){
    Object.values(currencyCollection).forEach(key=>{
        createTagElement('option', field);
        field[field.length - 1].value = key.code;
        field[field.length - 1].innerHTML = key.name;
    })
}

/**
 * Creates an HTML tag then appends it to the given parent
 * @param {String} tagName 
 * @param {HTML element} parent 
 */
function createTagElement(tagName, parent) {
	let tag = document.createElement(tagName);
	parent.appendChild(tag);
}

/**
 * adds convertion object to localstorage
 * @param {object} data 
 */
function addConversionToStorage(data){
    let dataJSON = JSON.stringify(data);
    localStorage.setItem(`"${localStorage.length}"`, dataJSON);
}