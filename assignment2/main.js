document.addEventListener('DOMContentLoaded', init);

/**
 * init function initiallizes event listeners for all buttons
 * and populates the select fields with options
 */
function init(){
    document.getElementById('get_data_btn').addEventListener('click', convertCurrency);
    document.getElementById('clear_storge_btn').addEventListener('click', killHistory);
    document.getElementById('read_storge_btn').addEventListener('click', showHistory);
    document.getElementById('get_dataXML_btn').addEventListener('click', convertCurrencyXML);

    loadCurrencyData();
}

/**
 * following class contains information fed by the user in order to
 * send a query to the target URL
 * 
 * getAbsoluteURL function returns an absolute URL
 * 
 * getData function processes the query 
 */
class URL{
    constructor(){
        this.fromQry = `from=${document.getElementById('base_currency_select').value}`;
        this.toQry   = `to=${document.getElementById('to_currency_select').value}`;
        this.amtQry  = `amount=${document.getElementById('amount').value}`;
        this.mainURL = 'https://api.exchangerate.host/convert';
    }

    getAbsoluteURL(){
        return `${this.mainURL}?${this.fromQry}&${this.toQry}&${this.amtQry}`;
    }

    getData(){
        fetch(this.getAbsoluteURL()).then(resp=>{
                if(resp.status === 200){
                    return resp.json();
                }
                throw new Error('query has failed');
            }).then(resp=>{
                let data = new ConversionData(resp);
                displayResults(data);
                addConversionToStorage(data);
                console.log('success');
            }).catch(err=>{
                console.log('oh no', err);
            })
    }
}


/**
 * following class extracts relevant data from getData()
 * which is later processed in displayResults()
 */
class ConversionData{
    constructor(resp){
        let time = new Date();
        this.from   = resp.query.from;
        this.to     = resp.query.to;
        this.rate   = Math.round(resp.info.rate * 100) / 100;
        this.amount = resp.query.amount;
        this.result = Math.round(resp.result * 100) / 100;
        this.date   = `${resp.date} - ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`
    }
}


/**
 * following function checks if the user input is valid
 * then it proceeds to send a request to its destination
 */
function convertCurrency(){  
    if(!validateCurrency()){
        alert("Invalid input.");
    }
    else{
        let url = new URL();
        url.getData();
    }
}

/**
 * same thing as above but done with XMLHttpRequest
 */
function convertCurrencyXML(){
    if(!validateCurrency()){
        alert("Invalid input.");
    }
    else{
        let url = new URL();
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url.getAbsoluteURL());
        xhr.send();
        xhr.addEventListener('load', ()=>{
            if(xhr.status === 200){
                let data = new ConversionData(JSON.parse(xhr.response));
                displayResults(data);
                addConversionToStorage(data);
            }
            else{
                console.log('oh no');
            }
        })
    }
}

/**
 * murders your entire history in localstorage
 */
function killHistory(){
    localStorage.clear();
    killTable();
}

/**
 * wipes the current table clean then
 * recreates the table with all data from localstorage
 */
function showHistory(){
    killTable();

    Object.values(localStorage).forEach(localData=>{
        let parsedData = JSON.parse(localData);
        displayResults(parsedData);
    })
}